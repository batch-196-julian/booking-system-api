/*
	TO be able to create routes from another file to be used in our application, from here, we have to import express as well, however, we will now use another method from express to contain our routes.
	the Router() method,will us to contain our routes
*/

const express = require("express");
const router = express.Router();

const courseControllers = require("../controllers/courseControllers");

const auth = require("../auth");

const {verify,verifyAdmin} = auth;

//All routes to courses now has an endpoint prefaced with /courses

//endpoint - /courses/
router.get('/',verify,verifyAdmin,courseControllers.getAllCourses);

//endpoint /courses/
router.post('/',verify,verifyAdmin,courseControllers.addCourse);

// Get 

router.get('/activeCourses',courseControllers.getActiveCourses);


// Activity

router.get('/getSingleCourse/:courseId',verify,verifyAdmin,courseControllers.getSingleCourse);


// update a single course
// Pass the id of the course we want to update via route params
// Update details will be passed via request body
router.put('/updateCourse/:courseId', courseControllers.updateCourse);

router.delete('/archiveCourse/:courseId',verify,verifyAdmin,courseControllers.archiveCourse);

module.exports = router;