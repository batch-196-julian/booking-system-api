const User = require("../models/User");

const Course = require ("../models/Course")

const bcrypt = require("bcrypt");

const auth = require("../auth");


module.exports.registerUser = (req,res)=>{


	const hashedPw = bcrypt.hashSync(req.body.password,10);
	console.log(hashedPw);



	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo

	})

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

}


module.exports.getUserDetails = (req,res)=>{
	console.log(req.user)
	//find() will return an array of documents which matches the criteria.
	//It will return an array even if it only found 1 document.
	/*User.find({_id:req.body.id})*/

	//findOne() will return a single document that matched our criteria.
	//User.findOne({_id:req.body.id})

	//findById() is a mongoose method that will allow us to find a document strictly by its id.
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.loginUser = (req,res) => {

	console.log(req.body);

	User.findOne({email:req.body.email})
	.then(foundUser => {

		//foundUser is the parameter that contains the result of findOne
		//findOne() returns null if it is not able to match any document
		if(foundUser === null){
			return res.send({message: "No User Found."})
			//Client will receive this object with our message if no user is found
		} else {
			//console.log(foundUser)
			//If we find a user, foundUser will contain the document that matched the input email.
			//Check if the input password from req.body matches the hashed password in our foundUser document.
			/*
				bcrypt.compareSync(<inputString>,<hashedString>)

				"spidermanOG"
				"$2b$10$ejEI0sqoMtvfdPpTfpzjjewB7525F0swxpXvatMD6nvS60WVTmVbm"

				If the inputString and the hashedString matches and are the same, the compareSync method will return true.
				else, it will return false.
			*/
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);

			//console.log(isPasswordCorrect);

			//IF the password if correct, we will create a "key", a token for our user, else, we will send a message: 
			if(isPasswordCorrect){

				/*
					To be able to create a "key" or token that allows/authorizes our logged in user around our application, we have to create our own module called auth.js.

					This module will create a encoded string which contains our users's details.

					This encoded string is what we call a JSONWebToken or JWT.

					This JWT can only be properly unwrapped or decoded with our own secret word/string.

				*/

				// console.log("We will create a token for the user if the password is correct")

				//auth.createAccessToken receives our foundUser document as an argument, gets only necessary details, wraps those details in JWT and our secret, then finally returns our JWT. This JWT will be sent to our client.
				return res.send({accessToken: auth.createAccessToken(foundUser)});
			} else {

				return res.send({message: "Incorrect Password"});
			}

		}

	})


}
// Activity

module.exports.getUserByEmail = (req,res)=>{
	// console.log(req.body)

	User.findOne({email:req.body.email})
	.then(result => {
		if (result === null){
			return res.send(false)		
		}else {
			return res.send(true)
		}	
	})
	.catch(error => res.send(error))

}

module.exports.enroll = async (req,res) => {

console.log(req.user.id);

console.log(req.body.courseId);

// Validate the user is an admin or not.

if (req.user.isAdmin){
	return res.send({message: "Action Forbidden."});
	}

	// async and await - async keyword is added to a function to make our function asynchronous. Which means that instead of JS regular behaviour of running 

	// return a boolean to our isUserUpdated variable to determine the result of the query an if 

	let isUserUpdated = await User.findById(req.user.id).then(user => {
		// console.log(user);

		let newEnrollment = {
			courseId: req.body.courseId

		}

		user.enrollments.push(newEnrollment)

		return user.save().then (user => true) .catch(err => err.message)

	})
	// console.log(isUserUpdated);

	if(isUserUpdated !== true){

		return res.send({message: isUserUpdated});

	}

	// find the course where we will enroll or add the user as an enrolee and return true IF we were able to push the user into the 

	let isCourseUpdated = await course.findById(req.body.courseId).then(course => {

		// console.log(course); // contain the found course

		let enrollee = {

			userId: req.user.id
		}

		// push the enrollee into the enrollees subdocument array of the course

		course.enrollees.push(employee);


		return course.save().then(course => true).catch(err => err.message);
		
	})
	// console.log(isCourseUpdated);

	if(isCourseUpdated !== true){
		return res.send({message: isCourseUpdated})
	} 
	// ensure that we are able to both update the user and a course document to add our enrollment and enrollee respectively 

	if(isUserUpdated && isCourseUpdated){
		return res.send({message: "Thank you for enrolling!!!"})
	}
}